terraform {
  required_version = ">= 1.1.9"
}

terraform {
  backend "s3" {
    bucket         = "prashant-terraform-state-storage-s3"
    key            = "prashant/terraform.tfstate"
    dynamodb_table = "prashant-tf-locks"
    region         = "us-west-2"
    encrypt        = true
  }
}

provider "aws" {
  region = var.region
}

module "prashant" {
  source             = "terraform-aws-modules/vpc/aws"
  name               = "prashant"
  cidr               = "10.0.0.0/16"
  public_subnets     = ["10.0.1.0/24", "10.0.2.0/24"]
  private_subnets    = ["10.0.3.0/24", "10.0.4.0/24"]
  azs                = ["${var.region}a", "${var.region}b"]
  enable_nat_gateway = "true"
  single_nat_gateway = true
  tags = {
    "Terraform"        = "true"
    "Environment"      = "prashant"
    "user:Team"        = "prashant"
    "user:Stack"       = "prashant"
    "user:Application" = "prashant"
  }
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 18.0"

  cluster_name    = "prashant"
  cluster_version = "1.23"

  cluster_endpoint_private_access = true

  node_security_group_additional_rules = {
    ingress_self_all = {
      description = "Node to node all ports/protocols"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }
    egress_all = {
      description      = "Node all egress"
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }

  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
    }
  }

  vpc_id     = module.prashant.vpc_id
  subnet_ids = module.prashant.private_subnets

  eks_managed_node_group_defaults = {
    disk_size      = 50
    instance_types = ["t2.medium"]
  }

  eks_managed_node_groups = {
    green = {
      min_size     = 1
      max_size     = 2
      desired_size = 1

      instance_types = ["t2.large"]
    }
  }
  manage_aws_auth_configmap = false

  tags = {
    Environment = "prashant"
    Terraform   = "true"
  }
}

module "rds" {
  source              = "../modules/rds"
  name                = var.name
  env                 = var.env
  db_password         = var.db_password
  db_user             = var.db_user
  vpc_id              = module.prashant.vpc_id
  private_subnets     = module.prashant.private_subnets
  db_engine           = var.db_engine
  db_engine_version   = var.db_engine_version
  db_instance_type    = var.db_instance_type
  db_storage          = var.db_storage
  snapshot_identifier = var.snapshot_identifier
  multi_az            = var.multi_az
}

resource "aws_security_group" "alb_sg" {
  vpc_id      = module.prashant.vpc_id

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = ["0.0.0.0/0"]
  }
}

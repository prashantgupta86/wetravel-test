variable "region" {
  default     = "us-west-2"
  description = "The AWS region to deploy to"
}

variable "db_engine" {
  default     = "mysql"
  description = "engine of DB"
}

variable "db_engine_version" {
  default     = "5.7.38"
  description = "engine version of DB"
}

variable "db_instance_type" {
  default     = "db.t3.medium"
  description = "instance type of DB"
}

variable "db_storage" {
  default     = "50"
  description = "storage of DB"
}

variable "snapshot_identifier" {
  default     = ""
  description = "snapshot of DB for creation"
}

variable "db_password" {
  default     = "12345678"
  description = "password of DB"
}

variable "db_user" {
  default     = "admin"
  description = "admin of DB"
}

variable "multi_az" {
  default     = "true"
  description = "multi az of DB"
}

variable "name" {
  default     = "prashant"
  description = "Name of application"
}

variable "env" {
  default     = "prashant"
  description = "enviroment for application"
}

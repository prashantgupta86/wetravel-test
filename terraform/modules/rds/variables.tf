variable "name" {
  default     = "innovations"
  description = "Name of application"
}

variable "env" {
  default     = "dev"
  description = "enviroment for DB"
}

variable "private_subnets" {
  type        = list(string)
  default     = []
  description = "private subnet of DB"
}

variable "db_engine" {
  default     = ""
  description = "engine of DB"
}

variable "db_engine_version" {
  default     = ""
  description = "engine version of DB"
}

variable "db_instance_type" {
  default     = ""
  description = "instance type of DB"
}

variable "snapshot_identifier" {
  default     = ""
  description = "snapshot of DB for creation"
}

variable "ec2_sg" {
  default     = ""
  description = "security group of ec2"
}

variable "vpc_id" {
  default     = ""
  description = "vpc of DB"
}

variable "db_storage" {
  default     = ""
  description = "storage of DB"
}

variable "db_password" {
  default     = ""
  description = "password of DB"
}

variable "db_user" {
  default     = ""
  description = "user of DB"
}

variable "multi_az" {
  default     = "false"
  description = "user of DB"
}

resource "aws_db_subnet_group" "db_subnet_group" {
    subnet_ids = "${var.private_subnets}"
    tags = {
        Name = "${var.env} DB subnet group"
    }
}

resource "aws_db_instance" "rds" {
  engine                      = "${var.db_engine}"
  engine_version              = "${var.db_engine_version}"
  identifier                  = "${var.name}-${var.env}-db"
  instance_class              = "${var.db_instance_type}"
  username                    = var.db_user
  password                    = "${var.db_password}"
  snapshot_identifier         = "${var.snapshot_identifier}"
  db_subnet_group_name        = "${aws_db_subnet_group.db_subnet_group.id}"
  vpc_security_group_ids      = ["${aws_security_group.db-sg.id}"]
  allocated_storage           = "${var.db_storage}"
  storage_encrypted           = true
  multi_az                    = var.multi_az
  apply_immediately           = true
  backup_retention_period     = 10
  auto_minor_version_upgrade  = false
  allow_major_version_upgrade = false
  copy_tags_to_snapshot       = true
  skip_final_snapshot         = true
}

resource "aws_security_group" "db-sg" {
    name = "db-sg"
    description = "DB security group"

    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    vpc_id = "${var.vpc_id}"
}
